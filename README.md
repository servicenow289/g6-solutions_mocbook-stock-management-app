**What is this?**
A user-friendly guide to importing the Mocbook Scoped Application into your ServiceNow instance.


**There are two Parts to importing Mocbook App into your Instance.**

**PART 1**
1. Create Basic Auth Credentials
2. All > System Applications > Studio
3. Under New Application window, select 'Import from Source Control'
4. Enter your credentials and the URL to the folder with sysid as name


**PART 2**
1. Navigate to ./XML Files.
2. Download the xml update sets locally.
3. In your instance, System Update Set > Retrieved Update Sets > Import Update Set from XML and commit changes as usual. 


**Additional Reference**: 
Application Development Fundamentals pg 101
Update Sets https://docs.servicenow.com/bundle/tokyo-application-development/page/build/system-update-sets/reference/update-set-transfers.html


**SUMMARY NOTE**  
- PART 1 imports all 'Mocbook' scoped data such as tables, forms, relationships, modules, ACLs, roles, flow and permissions. 
- PART 2 imports unscoped data such as Catalog Items, custom Service Portal design and widgets, custom Service Catalog, User and User Groups.


This App is built in ServiceNow San Diego Version.
